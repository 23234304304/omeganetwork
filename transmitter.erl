-module(transmitter).

% Exports
-export([gerarTransmitter/1, transmitter/2]).

% Imports
-import(auxiliaresOmega, [log2/1, rotacionarInteiro/3]).

% Gera os transmissores
gerarTransmitter(EnderecosNum) ->
  gerarTransmitter(EnderecosNum, 0)
.

% Condição de parada
gerarTransmitter(TransmitterNum, IteraTransmitter) when IteraTransmitter == TransmitterNum ->
  true;
% Gerar próximo transmissor
gerarTransmitter(TransmitterNum, IteraTransmitter) ->
  register(
    list_to_atom([0, IteraTransmitter]),
    spawn(transmitter, transmitter, [TransmitterNum, IteraTransmitter])
  ),
  gerarTransmitter(TransmitterNum, IteraTransmitter + 1)
.

% Processo transmissor
transmitter(TransmitterNum, Id) ->	
  NumBitsEndereco = trunc(log2(TransmitterNum)),
  SwitchDestino = rotacionarInteiro(Id, NumBitsEndereco, esquerda) div 2,
  Sucessor = list_to_atom([1, 2, SwitchDestino]),
  transmitter_comunicacao(Sucessor, Id)
.

% Loop transmissor
transmitter_comunicacao(Sucessor, Id) ->
  % O transmissor fica esperando a entrada do usuario (mensagem)
  receive
    {PID_Origem, EndDestino} when is_integer(EndDestino) ->
      Sucessor ! {self(), EndDestino},
      receive
      	{resposta, Dado} ->
          PID_Origem ! {resposta, lists:append(["Transmissor ", integer_to_list(Id), ", ", Dado])}
      end,
      transmitter_comunicacao(Sucessor, Id)
  end
.
