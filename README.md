# Descrição em português #

Simulador da Rede Omega de interconexão com múltiplos estágios, implementado na linguagem Erlang

# English description #

Omega Multi-stage Interconnection Network simulator implemented in the Erlang language