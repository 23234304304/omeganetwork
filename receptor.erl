-module(receptor).

% Exports
-export([gerarReceptor/1, receptor/1]).

% Gera os receptores
gerarReceptor(ReceptorNum)->
  gerarReceptor(ReceptorNum, 0)
.

% Condição de parada
gerarReceptor(ReceptorNum, IteraReceptor) when IteraReceptor == ReceptorNum -> true;
% Gerar próximo receptor
gerarReceptor(ReceptorNum, IteraReceptor)->
  register(
    list_to_atom([2, IteraReceptor]),
    spawn(receptor, receptor, [IteraReceptor])
  ),
  gerarReceptor(ReceptorNum, IteraReceptor + 1)
.

% Loop receptor
receptor(Num)->
  receive
    {PID_Origem, EndDestino} ->
      PID_Origem ! {resposta, lists:append("Receptor ", integer_to_list(Num))},
    receptor(Num)
  end
.
