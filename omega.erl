-module(omega).

% Exports
-export([inicializar/0]).

% Imports
-import(switch, [gerarSwitch/1]).
-import(receptor, [gerarReceptor/1]).
-import(transmitter, [gerarTransmitter/1]).

% Valida endereco
validarEndereco(Endereco) when is_number(Endereco), Endereco >= 0, Endereco < 8 ->
  true;
validarEndereco(Endereco) ->
  false
.

% Faz entrada
receberEntrada(TipoDeEndereco)->
  {ok, Endereco} = io:read(lists:append(["Insira o endereco de ", TipoDeEndereco, ": "])),
  case validarEndereco(Endereco) of
    false ->
      io:fwrite("Endereco invalido!~n", []),
      receberEntrada(TipoDeEndereco);
    true ->
      Endereco
  end
.

% Inicialização
inicializar() ->
  % Gera transmissores
  gerarTransmitter(8),

  % Gera switch
  gerarSwitch(8),

  % Gera receptores
  gerarReceptor(8),

  % Loop
  loop()
.

% Loop de transmissão c/ entrada
loop() ->
  % Entrada
  io:fwrite("Lembre-se: a entrada no Erlang termina em . e nao <enter>!~n", []),
  EnderecoOrigem = receberEntrada("origem"),
  EnderecoDestino = receberEntrada("destino"),

  % Envia mensagem para o nó transmissor, e a partir dele inicia-se a transmissão
  list_to_atom([0, EnderecoOrigem]) ! {self(), EnderecoDestino},

  % Aguarda resposta para printar o caminho
  receive
    {resposta, Dado} ->
      io:fwrite("Caminho percorrido: ~s.~n~n", [Dado])
  end,

  % Retorna loop
  loop()
.
