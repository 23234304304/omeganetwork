all:
	erl -compile auxiliaresOmega.erl
	erl -compile switch.erl
	erl -compile receptor.erl
	erl -compile transmitter.erl
	erl -compile omega.erl

run:
	@erl -run omega inicializar -noshell

tests:
	@erl -run omega inicializar -noshell < testInput.txt

clean:
	rm -rf *.beam *.dump
