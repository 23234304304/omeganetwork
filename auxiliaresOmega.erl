-module(auxiliaresOmega).

% Exports
-export([rotacionarInteiro/3, log2/1]).

% Imports
-import(math, [log/1, pow/2]).

% Log - base 2s
log2(X) ->
  log(X) / log(2).

% Rotacao 
rotacionarInteiro(Rotacionavel, BitsConsiderados, Sentido)
  when is_integer(Rotacionavel), is_integer(BitsConsiderados), Sentido == direita ->
    rotacionarInteiroDireita(Rotacionavel, trunc(pow(2, BitsConsiderados)));
rotacionarInteiro(Rotacionavel, BitsConsiderados, Sentido)
  when is_integer(Rotacionavel), is_integer(BitsConsiderados), Sentido == esquerda ->
    rotacionarInteiroEsquerda(Rotacionavel, trunc(pow(2, BitsConsiderados))).

% Rotacao direita
rotacionarInteiroDireita(Rotacionavel, Flag)
  when is_integer(Rotacionavel), is_integer(Flag), Rotacionavel rem 2 == 0 -> Rotacionavel bsr 1;
rotacionarInteiroDireita(Rotacionavel, Flag)
  when is_integer(Rotacionavel), is_integer(Flag) -> (Rotacionavel bsr 1) + (Flag bsr 1).

% Rotacao esquerda
rotacionarInteiroEsquerda(Rotacionavel, Flag)
  when is_integer(Rotacionavel), is_integer(Flag), Rotacionavel band (Flag bsr 1) == 0 -> (Rotacionavel bsl 1);
rotacionarInteiroEsquerda(Rotacionavel, Flag) when is_integer(Rotacionavel), is_integer(Flag) ->
  (Rotacionavel bsl 1) - Flag + (Rotacionavel bsl 1) div Flag.
