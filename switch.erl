-module(switch).

% Exports
-export([gerarSwitch/1, switch/4]).

% Imports
-import(math, [pow/2]).
-import(binary, [encode_unsigned/2]).
-import(file, [open/2]).
-import(auxiliaresOmega, [log2/1, rotacionarInteiro/3]).
  
% Gera os switches
gerarSwitch(EnderecosNum) ->
  EstagiosNum = log2(EnderecosNum),
  SwitchesNum = EnderecosNum div 2,
  gerarSwitch(EstagiosNum, SwitchesNum, 0, 0)
.

%Condição de parada para iterador de estagios
gerarSwitch(EstagiosNum, SwitchesNum, IteraEstagio, IteraSwitches) when IteraEstagio == EstagiosNum ->
  true;
%Condição de parada para iterador de switches
gerarSwitch(EstagiosNum, SwitchesNum, IteraEstagio, IteraSwitches) when IteraSwitches == SwitchesNum ->
  gerarSwitch(EstagiosNum, SwitchesNum, IteraEstagio + 1, 0);
%Gerar próximo switch
gerarSwitch(EstagiosNum, SwitchesNum, IteraEstagio, IteraSwitches) ->
  register(
    list_to_atom([1, IteraEstagio, IteraSwitches]),
    spawn(switch, switch, [EstagiosNum, SwitchesNum, IteraEstagio, IteraSwitches])
  ),
  gerarSwitch(EstagiosNum, SwitchesNum, IteraEstagio, IteraSwitches + 1)
.

% Cria as conexões de cada switch em tuplas
criarConexaoSucessor(NumBitsEndereco, Porta, Estagio) when Estagio == 0 ->
  {list_to_atom([2, Porta]), % próximo sem cruzamento
  list_to_atom([2, Porta + 1])} % próximo com cruzamento
;
criarConexaoSucessor(NumBitsEndereco, Porta, Estagio) ->
  % OBS: Os estagios são numerados em ordem decrescente para facilitar a verificação da flag.
  {list_to_atom([1, Estagio - 1, rotacionarInteiro(Porta, NumBitsEndereco, esquerda) div 2]), % próximo sem cruzamento
  list_to_atom([1, Estagio - 1, rotacionarInteiro(Porta + 1, NumBitsEndereco, esquerda) div 2])} % próximo com cruzamento
.

% Processo switch
switch(EstagiosNum, SwitchesNum, Estagio, Switch) ->
  NumBitsEndereco = trunc(log2(SwitchesNum)) + 1,
  % TODO: Avaliar necessidade: Antecessor = criarConexaoAntecessor(NumBitsEndereco, 2 * Switch, Estagio),
  Sucessor = criarConexaoSucessor(NumBitsEndereco, 2 * Switch, Estagio),
  switch_comunicacao(EstagiosNum, Sucessor, trunc(pow(2, Estagio)), Estagio, Switch)
.

% Loop switch
switch_comunicacao(EstagiosNum, Sucessor, FlagCruzamento, Estagio, Switch) ->
  receive
    % TODO: Não verifica se endereço de origem é válido
    {PID_Origem, EndDestino} when is_integer(EndDestino) ->
      element(((EndDestino band FlagCruzamento) bsr Estagio) + 1, Sucessor) ! {self(), EndDestino},
      receive {resposta, Dado} ->
        PID_Origem ! {resposta, "Switch " ++ integer_to_list(trunc(EstagiosNum-Estagio-1)) ++ "x" ++ integer_to_list(Switch ) ++ ", " ++ Dado}
      end,
      switch_comunicacao(EstagiosNum, Sucessor, FlagCruzamento, Estagio, Switch)
  end
.
